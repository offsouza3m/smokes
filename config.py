import json
from log import get_logger

logger = get_logger("smoke")

def write_config(data):
    json_string = json.dumps(data)
    with open('config.json', 'w') as outfile:
        outfile.write(json_string)

def read_config():
    data = None
    with open('config.json') as json_file:
        data = json.load(json_file)
    return data 

def check_config_file():
    try:
        data = read_config()
        print('***********************')
        conf, max_det, det_per_request, weights, ip_server, motion ,  motion_sensi, gui = data['conf'], data['max_det'], data['det_per_request'], data['weights'], data['ip_server'], data['motion'], data['motion_sensi'], data['gui']
        logger.info("reading config.json | {}".format(data))
    except Exception as e:
        print(e)
        logger.error("config.json not find ")
        logger.info("creating config.json")
        conf, max_det, det_per_request,weights,ip_server, motion, motion_sensi, gui= 0.4, 3, 1, './model/best.pt', 'localhost', True, 0.001, True
        data = dict(conf = conf, max_det=max_det, det_per_request=det_per_request, weights= weights, ip_server=ip_server, motion=motion,  motion_sensi= motion_sensi, gui=gui)
        write_config(data)
    return conf, max_det, det_per_request,weights,ip_server, motion, motion_sensi, gui