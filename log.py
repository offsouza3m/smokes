from __future__ import absolute_import

import logging

from os import mkdir
from os.path import exists


def get_logger(module_name):

    log_file = './logs/{}.log'.format(module_name)
    if not exists(log_file):
        if not exists('./logs'): mkdir('./logs')
        app_logs = open(log_file, 'w')
        app_logs.close()
    log_wp = logging.getLogger(module_name)
    formatter= logging.Formatter('%(asctime)s %(filename)s(%(lineno)d) - %(levelname)s : %(message)s')
    fhdlr = logging.FileHandler(log_file, encoding='utf-8')
    fhdlr.setFormatter(formatter)
    log_wp.addHandler(fhdlr)
    log_wp.setLevel(logging.INFO)
    return log_wp
