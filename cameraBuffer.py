import cv2
from threading import Event, Thread
from time import sleep, time
from log import get_logger

logger = get_logger("smoke")

class CameraBufferCleanerThread(Thread):
    """ Thread responsável de atualizar os frames das câmeras em tempo real"""
    def __init__(self, camera_ip, name='camera-buffer-cleaner-thread'):
        self.camera_ip = camera_ip
        self.cam_id = name
        self.kill = False
        self.last_frame = None
        self.uptime = time()
        self.last_cap = time()
        super(CameraBufferCleanerThread, self).__init__(name=name)
        self.start()
        self.alive = False

    def run(self):
        logger.info('Tentando conectar camera: {}'.format(self.cam_id))
        cap = cv2.VideoCapture(self.camera_ip)
        if cap.isOpened():
            logger.info('Conectado: {}'.format(self.cam_id))
            self.alive = True
            while self.kill == False:
                    sleep(0.01)
                    ret , frame = cap.read()                
                    if ret:
                        self.last_frame = frame
                        self.last_cap = time()
                    else:
                        self.last_frame = None
        self.alive = False

    