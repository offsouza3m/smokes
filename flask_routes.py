from flask import Flask, request, jsonify
from threading import Event, Thread
import cv2
from time import sleep, time
from log import get_logger
from cameras_control import Cameras_control
from config import write_config, read_config
from detector import SmokeDetector
from motion_detector import MotionDetector

logger = get_logger("smoke")
cam_ctrl = Cameras_control()
tracking_app = Flask(__name__)
smoke_det = SmokeDetector()

det_per_request = 1

@tracking_app.route('/', methods=['GET'])
def status():
    return 'ok'

@tracking_app.route('/show', methods=['GET'])
def show_video():
    """
    Habilita a visualização das detecções quando a rota /verify for acionada
    Usar somente para debugar
     """
    cam_ctrl.show = True
    return 'show'

@tracking_app.route('/noshow', methods=['GET'])
def noshow():    
    """
    Desabilita a visualização das detecções quando a rota /verify for acionada
    Usar somente para debugar
     """
    cam_ctrl.show = False
    return 'noshow'


@tracking_app.route('/activate_cameras', methods=['POST'])
def activate_cameras():    
    """
    Habilita a câmera informada para realizar a detecção de fumaça
    Retorna um json informando se a câmera foi conectada com sucesso

    :param: json
    {
        id : int,
        ip : string -> câmera ip
        user: string
        password: string
        port: int
    } 
    """
    cam = request.json   

    if not cam:
        logger.info("send id, ip, user, password")
        return dict(code=3,result="send id, ip, user, password ")

    if 'id' not in cam:
        logger.info("send id, ip, user, password")
        return dict(code=3,result="send id camera ")
    if 'ip' not in cam:
        logger.info("send ip  camera")
        return dict(code=3,result="send ip camera ")
    if 'user' not in cam:
        logger.info("send user  camera")
        return dict(code=3,result="send user camera ")
    if 'password' not in cam:
        logger.info("send password  camera")
        return dict(code=3,result="send password camera ")
    if 'port' not in cam:
        logger.info("send port  camera")
        return dict(code=3,result="send port camera ")
   
    activate = cam_ctrl.add_camera(cam)

    if activate:
        response = dict(code=0,result="camera conected")
        logger.info("Camera {} conected * ".format(cam['id']))
        return jsonify(response)
    else:
        response = dict(code=1,result="camera not conected")
        logger.info("Camera {} not conected".format(cam['id']))
        return jsonify(response)

@tracking_app.route('/deactivate_camera', methods=['POST'])
def deactivate():
    """
    Desabilita a câmera informada para realizar a detecção de fumaça
    Retorna um json informando se a câmera foi desconectada com sucesso

    :param: json
    {
        id : int        
    } 
    """
    cam = request.json
    if not cam:
        return dict(code=3,result="id not received")
    if 'id' not in cam:
        response = dict(code=2,result="id key not find")
        return jsonify(response)
    elif cam['id'] not in cam_ctrl.cameras_conected:
        response = dict(code=1,result="camera id {} not find".format(cam['id']))
        return jsonify(response)
    if cam_ctrl.cameras_conected[cam['id']]['stream'] is not None:
        cam_ctrl.cameras_conected[cam['id']]['stream'].kill = True
        sleep(0.1)
    del cam_ctrl.cameras_conected[cam['id']]
    response = dict(code=0,result="camera {} removed".format(cam['id']))
    logger.info("camera {} removed".format(cam['id']))    
    return jsonify(response)



@tracking_app.route('/verify', methods=['POST','GET'])
def check():       
    """
    Solicita uma verificação de fumaça na câmera indicada
    Retorna um json informando se houve detecção na câmera
    :param: json
    {
        id: int
        width: int
        height: int
    } 
    """
    cam = request.json    
    if not cam:
        return dict(code=3,result="id not received")
    if 'id' not in cam:
        response = dict(code=2,result="id key not find")
        return jsonify(response)
    elif cam['id'] not in cam_ctrl.cameras_conected:
        response = dict(code=1,result="camera id {} not find".format(cam['id']))
        return jsonify(response)
    if 'width' not in cam or 'heigth' not in cam:
        return dict(code=3,result="width or heigth not received")    
    
    alert, detections,time_det = False, None, None
    t0 = time()
    for _ in range(0,det_per_request):        
        frame = cam_ctrl.cameras_conected[cam['id']]['stream'].last_frame 
        if len(frame) <=0: return dict(code=3,result="try reconected camera:{} ".format(cam['id']))    
        frame = cv2.resize(frame, (cam['width'], cam['heigth']))
        frame_res = cv2.resize(frame, (640, 640))
        t1 = time()
        dets, im0 = smoke_det.detect(frame_res, frame)
        print('time detect: ', time() - t1)
        print(dets)
        if cam_ctrl.show == True and cam_ctrl.cam_show is None:
            print('show')
            show_thread = Thread(target=cam_ctrl.show_img) 
            show_thread.start()   
            cam_ctrl.img_show = im0
            cam_ctrl.cam_show = cam['id']
        elif cam_ctrl.show:
            cam_ctrl.img_show = im0
            cam_ctrl.cam_show = cam['id']
       
        if len(dets['dets']) > 0 :
                logger.info("Detected camera {}".format(cam))
                alert= True
                detections= dets['dets']
                time_det=dets['time']    
                detections = MotionDetector(threshold=smoke_det.motion_sensi).detector(cam_ctrl.cameras_conected[cam['id']]['stream'], detections) if smoke_det.motion else detections

                break 
    response = dict(alert=alert, dets= detections, time=time_det)
    print('TIME Request: ', time() - t0)
    return jsonify(response)


@tracking_app.route('/setings', methods=['POST','GET'])
def config():
    global det_per_request, smoke_det
    camera = request.json
    if not camera:
        return dict(code=3,result="not received")
    if 'conf' not in camera:
        response = dict(code=2,result="CONF key not find")
        return jsonify(response)
    if 'verify_frames' not in camera:
        response = dict(code=2,result="verify_frames key not find")
        return jsonify(response)

    smoke_det.conf_thres = camera['conf']
    smoke_det.max_det = camera['max_det']
    det_per_request = camera['verify_frames']
    weights = read_config()['weights']    
    data = dict(conf = camera['conf'], max_det= camera['max_det'], det_per_request=camera['verify_frames'], weights=weights)
    write_config(data)
    return jsonify("update setings: Confiabilidade: {}, det_per_request: {}, Max Detections: {}".format(smoke_det.conf_thres, det_per_request,  smoke_det.max_det ))