import sys
sys.path.insert(0, './yolov5')

from yolov5.models.common import DetectMultiBackend
from yolov5.models.experimental import attempt_load
from yolov5.utils.datasets import letterbox
from yolov5.utils.general import check_img_size, non_max_suppression, scale_coords, check_imshow, xyxy2xywh
from yolov5.utils.torch_utils import select_device, time_sync
from yolov5.utils.plots import Annotator, colors, save_one_box
from datetime import datetime
import cv2
import torch
import torch.backends.cudnn as cudnn
import numpy as np
from time import sleep, time
from log import get_logger

logger = get_logger("smoke")

model = None
device = ""
class SmokeDetector():
    """
    Singleton Class
    Inicia o modelo YOLO e realizar a predição do modelo treinado
    """
    class_control = None

    def __new__(cls):
        if cls.class_control is None:  
          cls.class_control = super(SmokeDetector,   cls).__new__(cls)
          return cls.class_control
        else: return cls.class_control

    def init(self, weights= './model/best.pt',conf_thres=0.25, iou_thres=0.45, max_det=3, 
                        imgsz= 640, half= True, augment= False, visualize=False, device='cpu',
                        classes=None, hide_conf = False, hide_labels = False,agnostic_nms=False,dnn=False, motion=True, motion_sensi= 0.01):
        self.half=half
        self.augment=augment
        self.visualize=visualize
        self.device = device      
        self.weights = weights
        self.imgsz = imgsz
        self.conf_thres=conf_thres
        self.iou_thres=iou_thres
        self.classes=classes
        self.agnostic_nms=agnostic_nms
        self.dnn=dnn
        self.max_det=max_det
        self.hide_conf = hide_conf
        self.hide_labels = hide_labels
        self.model = None
        self.dt, self.seen = [0.0, 0.0, 0.0], 0
        self.isloaded = False
        self.motion = motion
        self.motion_sensi = motion_sensi


    def load_model(self):
        print(self.weights)
        self.device = select_device(self.device)
        self.model = DetectMultiBackend(self.weights, device=self.device, dnn=self.dnn)
        stride, self.names, pt, jit, onnx = self.model.stride, self.model.names, self.model.pt, self.model.jit, self.model.onnx
        imgsz = check_img_size(self.imgsz, s=stride) 
        self.half &= pt and self.device.type != 'cpu' 
        if pt:
            self.model.model.half() if self.half else self.model.model.float()
        if pt and self.device.type != 'cpu':
            self.model(torch.zeros(1, 3, *imgsz).to(self.device).type_as(next(self.model.model.parameters()))) 
        self.isloaded = True
        logger.info("load model")    

    def detect(self, frame_ori_res, frame_ori):
        
        frame = letterbox(frame_ori_res, self.imgsz, stride=32)[0]
        frame = frame.transpose(2, 0, 1)
        frame = np.ascontiguousarray(frame)   
        t1 = time_sync()
        im = torch.from_numpy(frame).to(self.device)
        im = im.half() if self.half else im.float()  
        im /= 255 
        if len(im.shape) == 3:
            im = im[None]
        t2 = time_sync()
        self.dt[0] += t2 - t1     
        pred = self.model(im, augment=self.augment, visualize=self.visualize)
        t3 = time_sync()
        self.dt[1] += t3 - t2
        pred = non_max_suppression(pred, self.conf_thres, self.iou_thres, self.classes, self.agnostic_nms, max_det=self.max_det)
        self.dt[2] += time_sync() - t3
        lista_pred = list()
        dict_pred = dict()       
        for i, det in enumerate(pred): 
            im0 = frame_ori           
            annotator = Annotator(im0, line_width=2, example=str(self.names))
            if len(det):               
                det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()                
                for *xyxy, conf, cls in reversed(det):
                    c = int(cls) 
                    label = None if self.hide_labels else (self.names[c] if self.hide_conf else f'{self.names[c]} {conf:.2f}')
                    annotator.box_label(xyxy, label, color=colors(c, True))                    
                    x1,y1,x2,y2 = [int(xyxy[0]), int(xyxy[1]), int(xyxy[2]), int(xyxy[3])] #convert xyxy to xywh
                    x,y,w,h = x1, y1 , x2-x1, y2-y1
                    lista_pred.append( {"xywh":[x,y,w,h], "conf":float(conf)})        
        dict_pred['dets'] = lista_pred
        dict_pred['time'] = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")     
        return dict_pred, im0
        