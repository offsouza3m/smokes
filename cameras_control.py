from cameraBuffer import CameraBufferCleanerThread
from time import sleep, time
from cv2 import imshow, waitKey, destroyAllWindows
from log import get_logger

logger = get_logger("smoke")

class Cameras_control():
    """Singleton Class, responsável por gerenciar todas as câmeras"""
    class_control = None
    cameras_conected = dict()
    show = False
    cam_show = None
    img_show = None
    def __new__(cls):
        if cls.class_control is None:  
          cls.class_control = super(Cameras_control,   cls).__new__(cls)
          return cls.class_control
        else: return cls.class_control

    def show_img(cls):        
        while True:
            if cls.cam_show is None or cls.img_show is None:
                continue            
            imshow('camera_{}'.format(cls.cam_show), cls.img_show)
            if waitKey(33) == ord('q'):
                break       
            if cls.show == False:
                destroyAllWindows()            
                cls.img_show = None
                cls.cam_show = None
                break

    def add_camera(cls, cam): 
        """ Abre o stream da câmera e a deixa disponivel no sistema """
        activate = False    
        id = cam['id']
        ip = cam['ip']
        user = cam['user']
        port = cam['port']
        password = cam['password']
        rtsp = 'rtsp://{}:{}@{}:{}/cam/realmonitor?channel=1&subtype=0'.format(user,password,ip,port)
        stream = CameraBufferCleanerThread(camera_ip=rtsp, name=str(id))
        activate = False
        if stream:
            timeout = time()
            while time() - timeout < 15:
                sleep(1)
                if stream.alive:  
                    cls.cameras_conected[id] = {'stream': stream, 'id':id, 'user': user, 'password':password, 'ip':ip, 'detections': {} }                
                    if 'id' in cls.cameras_conected and cls.cameras_conected[id]['stream'] is not None :
                        logger.info('camera {} foi conectada'.format(cam))                    
                    activate = True
                    break
        else:
            activate = False    
        return activate
    